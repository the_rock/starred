import argparse
import glob
import numpy as np
import matplotlib.pyplot as plt
import os

from copy import deepcopy

from starred.deconvolution.deconvolution import Deconv
from starred.deconvolution.loss import Loss
from starred.deconvolution.parameters import ParametersDeconv
from starred.plots import f2n
from starred.utils.generic_utils import save_fits
from starred.utils.optimization import Optimizer
from starred.utils.noise_utils import propagate_noise

args = None
parser = argparse.ArgumentParser(description='Deconvolution Options')
parser.add_argument('--subsampling_factor', dest='subsampling_factor', type=int,
                    help='The subsampling factor.')
parser.add_argument('--niter', dest='n_iter', type=int,
                    default=60, help='Number of iterations')
parser.add_argument('--lambda_scales', dest='lambda_scales', type=float,
                    default=1, help='Low and medium frequencies regularization parameter.')
parser.add_argument('--lambda_hf', dest='lambda_hf', type=float,
                    default=1, help='High frequency regularization parameter.')
parser.add_argument('--noise_map', dest='noise_map_provided', action='store_true', 
                    default=False, help='True if noise map is being provided.')
parser.add_argument('--no-noise-propagation', dest='prop_noise', action='store_false',
                    default=True, help='To skip the propagation of gaussian and Poisson noise.')
parser.add_argument('-g', dest='g', type=float, 
                    default=1, help='Instrument gain in ADU/electron.')
parser.add_argument('-t', dest='t_exp', type=float, 
                    default=1, help='Exposure time in seconds.')
parser.add_argument('--point_sources', dest='point_sources', type=int,
                    help='Number of point sources present in the data.')
parser.add_argument('--cx', nargs='+', default=[], help='Horizontal positions of the point sources')
parser.add_argument('--cy', nargs='+', default=[], help='Vertical positions of the point sources')
parser.add_argument('--data_path', dest='data_path', type=str, default='./')
parser.add_argument('--noise_map_path', dest='noise_map_path', type=str, default='./')
parser.add_argument('--psf_path', dest='psf_path', type=str, default='./')
parser.add_argument('--output_path', dest='output_path', type=str, default='./')


arguments = parser.parse_args()

def main(args):

    # Parameters
    subsampling_factor = args.subsampling_factor
    n_iter = args.n_iter
    M = args.point_sources
    lambda_scales = args.lambda_scales
    lambda_hf = args.lambda_hf
    gain = args.g
    t_exp = args.t_exp
    method = 'trust-constr'
    convolution_method = 'fft'
    prop_noise = args.prop_noise
    if M != 0:
        n_iter_first = n_iter // 4
    else:
        n_iter_first = 0

    # Retrieving data
    file_paths = sorted(glob.glob(os.path.join(args.data_path, '*.npy')))
    data = np.array([np.load(f) for f in file_paths]) * t_exp / gain
    im_size = np.shape(data)[1]
    epochs = len(file_paths)
    im_size_up = im_size * subsampling_factor
    
    # Noise map
    if args.noise_map_provided:
        sigma_paths = sorted(glob.glob(os.path.join(args.noise_map_path, '*.npy')))
        sigma_2 = np.array([np.load(f) for f in sigma_paths]) ** 2
    else:
        sigma_2 = np.zeros((epochs, im_size, im_size))
        sigma_sky_2 = np.array([np.std(data[i,int(0.9*im_size):,int(0.9*im_size):]) for i in range(epochs)]) ** 2
        for i in range(epochs):
            sigma_2[i,:,:] = sigma_sky_2[i] + data[i,:,:].clip(min=0) 

    # Parameter initialization
    initial_c_x = np.array(args.cx * subsampling_factor)
    initial_c_y = np.array(args.cy * subsampling_factor)
    initial_a = np.array([data[i,:,:].max() for j in range(M) for i in range(epochs)]) / 10e3
    scale = data.max()
    initial_h = np.zeros((im_size_up**2))

    print(initial_c_x)
    print(initial_c_y)
    print(initial_a)

    # The translations dx, dy are set to zero for the first epoch.
    # Thus we only initialize (epochs-1) of them.
    kwargs_init = {
        'kwargs_analytic': {
                            'c_x': initial_c_x,
                            'c_y': initial_c_y,
                            'dx' : np.ravel([0. for _ in range(epochs-1)]),
                            'dy' : np.ravel([0. for _ in range(epochs-1)]),
                            'a'  : initial_a},
        'kwargs_background': {'h': initial_h,
                            'mean': np.ravel([0. for _ in range(epochs)])},
    }
    kwargs_fixed = {
        'kwargs_analytic': {
            'c_x': initial_c_x, 
            'c_y': initial_c_y, 
            # 'a'  : initial_a
        },
        'kwargs_background': {'h': initial_h},
    }

    kwargs_up = {
        'kwargs_analytic': {'c_x': list([im_size_up/2. for i in range(M)]),
                            'c_y': list([im_size_up/2. for i in range(M)]),
                            'dx' : [5 for _ in range(epochs-1)],
                            'dy' : [5 for _ in range(epochs-1)],
                            'a': list([np.inf for i in range(M*epochs)]) 
                        },
        'kwargs_background': {'h': list([np.inf for i in range(0, im_size_up**2)]),
                            'mean': [np.inf for _ in range(epochs)]
                            },
    }

    kwargs_down = {
        'kwargs_analytic': {'c_x': list([-im_size_up/2. for i in range(M)]),
                            'c_y': list([-im_size_up/2. for i in range(M)]),
                            'dx' : [-5 for _ in range(epochs-1)],
                            'dy' : [-5 for _ in range(epochs-1)],
                            'a': list([0 for i in range(M*epochs)]) },
        'kwargs_background': {'h': list([-np.inf for i in range(0, im_size_up**2)]),
                            'mean': [-np.inf for _ in range(epochs)]
                            },
            }

    # Retrieving the PSF 

    file_paths = sorted(glob.glob(os.path.join(args.psf_path, '*.npy')))
    s = np.array([np.load(f) for f in file_paths])

    # Getting the model
    model = Deconv(image_size=im_size, 
                number_of_sources=M, 
                scale=scale, 
                upsampling_factor=subsampling_factor, 
                epochs=epochs, 
                psf=s, 
                convolution_method=convolution_method)

    parameters = ParametersDeconv(model, kwargs_init, 
                                kwargs_fixed, 
                                kwargs_up=kwargs_up, 
                                kwargs_down=kwargs_down)

    # Tunning amplitudes and point source positions
    loss = Loss(data, model, parameters, sigma_2, regularization_terms='l1_starlet', regularization_strength_scales=0, regularization_strength_hf=0)   
    optim = Optimizer(loss, parameters, method=method)
    optim.minimize(maxiter=n_iter_first, restart_from_init=True, use_grad=True, use_hessian=False, use_hvp=False) 

    # Printing partial results
    kwargs_partial = deepcopy(parameters.best_fit_values(as_kwargs=True))
    print(kwargs_partial)

    if prop_noise:
        print('Computing noise propagation...')
        W = propagate_noise(model, np.sqrt(sigma_2), kwargs_partial, wavelet_type_list=['starlet'], method='SLIT',
                            num_samples=5000,
                            seed=1, likelihood_type='chi2', verbose=False, upsampling_factor=subsampling_factor)[0]
    else:
        W = None

    # Background tunning
    kwargs_fixed = {
        'kwargs_analytic': {},
        'kwargs_background': {},
    }

    parameters = ParametersDeconv(model, 
                                kwargs_init=kwargs_partial, 
                                kwargs_fixed=kwargs_fixed, 
                                kwargs_up=kwargs_up, 
                                kwargs_down=kwargs_down)

    loss = Loss(data, model, parameters, sigma_2, 
                regularization_terms='l1_starlet', 
                regularization_strength_scales=lambda_scales, 
                regularization_strength_hf=lambda_hf, W=W) 

    optim = Optimizer(loss, parameters, method=method)

    optim.minimize(maxiter=n_iter, restart_from_init=False, 
                use_grad=True, use_hessian=False, use_hvp=True) 


    # Printing final results
    kwargs_final = deepcopy(parameters.best_fit_values(as_kwargs=True))
    print(kwargs_final)
    
    # Retrieving different elements of the deconvolved image
    epoch = 0
    output = model.model(kwargs_final)[epoch] 
    deconv, h = model.getDeconvolved(kwargs_final, epoch)
    deconv, h = deconv * gain / t_exp, h * gain / t_exp

    data_show = data[epoch,:,:] 

    dif = data_show - output
    rr = np.abs(dif) / np.sqrt(sigma_2[epoch,:,:])
    data_show *= gain / t_exp 
    output *= gain / t_exp 

    # Plotting
    fig = plt.figure(figsize=(12, 12))
    
    f2n.spplot(i=1, array=data[epoch,:,:]*gain/t_exp, fig=fig, title="Data")
    f2n.spplot(i=2, array=output*gain/t_exp, fig=fig, title="Convolving back")
    f2n.spplot(i=3, array=rr, fig=fig, title="Relative residuals")
    f2n.spplot(i=4, array=h, fig=fig, title="Extended sources")
    f2n.spplot(i=5, array=deconv, fig=fig, title="Deconvolved image")
    f2n.spplot(i=6, array=s[epoch,:,:], fig=fig, title="PSF")
    plt.savefig(args.output_path + '/deconv_output.png')

    # Generating the fits files
    save_fits(deconv, args.output_path + 'out')
    save_fits(h, args.output_path + 'background')
    save_fits(data[epoch,:,:], args.output_path + 'data')
    save_fits(output, args.output_path + 'cback')
    save_fits(rr, args.output_path + 'residuals')
    
if __name__ == '__main__':
    main(arguments)