import argparse
from copy import deepcopy
import glob
import numpy as np
import os
import matplotlib.pyplot as plt
import pickle as pkl
from astropy.io import fits

from starred.plots import f2n
from starred.psf.psf import PSF
from starred.psf.loss import Loss
from starred.psf.parameters import ParametersPSF
from starred.utils.generic_utils import save_fits
from starred.utils.optimization import Optimizer
from starred.utils import ds9reg
from starred.plots import plot_function as ptf
from starred.utils.noise_utils import propagate_noise
from jax.config import config; config.update("jax_enable_x64", True)

args = None
parser = argparse.ArgumentParser(description='NarrowPSFGen Options')
parser.add_argument('--subsampling_factor', dest='subsampling_factor', type=int,
                    default=2, help='The subsampling factor.')
parser.add_argument('--niter', dest='n_iter', type=int,
                    default=60, help='Number of iterations')
parser.add_argument('--lambda_scales', dest='lambda_scales', type=float,
                    default=1, help='Low and medium frequencies regularization parameter.')
parser.add_argument('--lambda_hf', dest='lambda_hf', type=float,
                    default=1, help='High frequency regularization parameter.')
parser.add_argument('--lambda_pos', dest='lambda_pos', type=float,
                    default=0.0, help='Positivity constraint. 0 do not impose positivity.')
parser.add_argument('--noise_map', dest='noise_map_provided', action='store_true', 
                    default=False, help='True if noise map is being provided.')
parser.add_argument('--no-noise-propagation', dest='prop_noise', action='store_false',
                    default=True, help='To skip the propagation of gaussian and Poisson noise.')
parser.add_argument('-g', dest='g', type=float, 
                    default=1, help='Instrument gain in ADU/electron.')
parser.add_argument('-t', dest='t_exp', type=float, 
                    default=1, help='Exposure time in seconds.')
parser.add_argument('--data_path', dest='data_path', type=str, default='./', help='Path to the data (str).')
parser.add_argument('--noise_map_path', dest='noise_map_path', type=str, default='./', help='Path to the noise maps (str).')
parser.add_argument('--output_path', dest='output_path', type=str, default='./', help='Path to save the output files and plots (str).')
parser.add_argument('--plot-interactive', dest='plot_interactive', action='store_true', default=False,
                    help='Display plot if True')
parser.add_argument('--no_bkg_fit', dest='no_bkg_fit', action='store_true', default=False,
                    help='No background fit if True')
parser.add_argument('--regularize_full_psf', dest='regularize_full_psf', action='store_true', default=False,
                    help='Regularize the full psf (Moffat+background) if True')
parser.add_argument('--method', dest='method', type=str, default='SLIT', help='Method to propagate noise. "SLIT" or "MC".')

arguments = parser.parse_args()

def main(args):
    # Parameters
    subsampling_factor = args.subsampling_factor
    n_iter = args.n_iter
    n_iter_initial = 40
    gain = args.g
    t_exp = args.t_exp
    lambda_scales = args.lambda_scales
    lambda_hf = args.lambda_hf
    lambda_positivity = args.lambda_pos
    method_analytical = 'trust-constr'
    method = 'adabelief'
    prop_noise = args.prop_noise
    # method = 'Newton-CG'

    # Data 
    file_paths_npy = sorted(glob.glob(args.data_path + '*.npy'))
    file_paths_fits = sorted(glob.glob(args.data_path + '*.fits'))
    new_vignets_data = np.array([np.load(f) for f in file_paths_npy]) * t_exp / gain
    if len(new_vignets_data) == 0 :
        new_vignets_data = np.array([fits.open(f)[0].data for f in file_paths_fits]) * t_exp / gain
    else :
        if len(file_paths_fits) > 0 :
            new_vignets_data += np.array([fits.open(f)[0].data for f in file_paths_fits]) * t_exp / gain

    N = len(file_paths_npy) + len(file_paths_fits) # number of stars
    if N > 0:
        print('Running joined fit on %i images...'%N)
    else:
        raise RuntimeError('No .fits or .npy files found.')

    image_size = np.shape(new_vignets_data)[1] # data dimensions
    image_size_up = image_size * subsampling_factor 

    # Masking
    masks = np.ones((N, image_size, image_size))
    for i in range(N):
        possiblemaskfilepath = os.path.join(args.noise_map_path, 'mask_%s.reg'%str(i))
        if os.path.exists(possiblemaskfilepath):
            reg = ds9reg.Regions(image_size, image_size)
            reg.readds9(possiblemaskfilepath, verbose=False)
            reg.buildinvertedmask(verbose=False)
            masks[i,:,:] = reg.nomask

    # Noise map
    if args.noise_map_provided:
        sigma_paths_npy = sorted(glob.glob(args.noise_map_path + '*.npy'))
        sigma_paths_fits = sorted(glob.glob(args.noise_map_path + '*.fits'))
        sigma_2 = np.array([np.load(f) for f in sigma_paths_npy]) ** 2
        if len(sigma_2) == 0:
            sigma_2 = np.array([fits.open(f)[0].data for f in sigma_paths_fits])**2
        else:
            if len(sigma_paths_fits) > 0:
                sigma_2 += np.array([fits.open(f)[0].data for f in sigma_paths_fits])**2

        if len(sigma_2) == N:
            print('Found %i noise maps...' %len(sigma_2))
        else:
            raise RuntimeError('Noise maps not found.')
    else:
        print('Computing noise maps...')
        sigma_2 = np.zeros((N, image_size, image_size))
        sigma_sky_2 = np.array([np.std(new_vignets_data[i,int(0.9*image_size):,int(0.9*image_size):]) for i in range(N)]) ** 2
        for i in range(N):
            sigma_2[i,:,:] = sigma_sky_2[i] + new_vignets_data[i,:,:].clip(min=0)

    # Renormalise your data and the noise maps by the max of the first image. Works better when using adabelief
    norm = new_vignets_data[0].max() / 100.
    new_vignets_data /= norm
    sigma_2 /= norm ** 2

    # Calling the PSF model
    model = PSF(image_size=image_size, number_of_sources=N,
                upsampling_factor=subsampling_factor,
                convolution_method='fft')

    # Parameter initialization
    kwargs_init, kwargs_fixed, kwargs_up, kwargs_down = model.smart_guess(new_vignets_data, fixed_background=True, guess_method='max')
    print('Initial guess :', kwargs_init)
    parameters = ParametersPSF(model, kwargs_init, kwargs_fixed, kwargs_up=kwargs_up, kwargs_down=kwargs_down)

    if args.plot_interactive:
        fig = ptf.display_data(new_vignets_data, sigma_2=sigma_2, units='e-',
                               center=(kwargs_init['kwargs_gaussian']['x0'], kwargs_init['kwargs_gaussian']['y0']))
        plt.show()

    # Moffat fitting and amplitude tunning
    print('Fitting Moffat ...')
    loss = Loss(new_vignets_data, model, parameters, sigma_2, N, masks=masks, regularization_terms='l1_starlet',
                regularization_strength_scales=0, regularization_strength_hf=0)

    optim_analytical = Optimizer(loss, parameters, method=method_analytical)
    optim_analytical.minimize(maxiter=n_iter_initial, restart_from_init=True,
                                use_grad=True, use_hessian=False, use_hvp=True)

    # Printing partial results    
    kwargs_bestfit = parameters.best_fit_values(as_kwargs=True)
    print('Partial results : ', kwargs_bestfit)


    fig1 = ptf.single_PSF_plot(model, new_vignets_data, sigma_2, kwargs_bestfit, n_psf=0, units='e-')
    fig2 = ptf.multiple_PSF_plot(model, new_vignets_data, sigma_2, kwargs_bestfit, units='e-')
    fig1.savefig(os.path.join(args.output_path, 'single_PSF_plot_partial.png'))
    fig2.savefig(os.path.join(args.output_path, 'multit_PSF_plot_partial.png'))

    if args.plot_interactive:
        plt.show()

    if prop_noise and not args.no_bkg_fit:
        print('Computing noise propagation...')
        W = propagate_noise(model, np.sqrt(sigma_2), kwargs_bestfit, wavelet_type_list=['starlet'], method=args.method,
                            num_samples=500,
                            seed=1, likelihood_type='chi2', verbose=False, upsampling_factor=subsampling_factor)[0]
    else:
        W = None

    # Background tuning, fixing Moffat
    kwargs_fixed = {
        'kwargs_moffat': {'fwhm': kwargs_bestfit['kwargs_moffat']['fwhm'],
                          'beta': kwargs_bestfit['kwargs_moffat']['beta'], 'C': kwargs_bestfit['kwargs_moffat']['C']},
        'kwargs_gaussian': {},
        'kwargs_background': {},
    }

    if not args.no_bkg_fit:
        print('Fitting background...')
        parameters = ParametersPSF(model, kwargs_bestfit, kwargs_fixed, kwargs_up = kwargs_up,  kwargs_down =kwargs_down)
        loss = Loss(new_vignets_data, model, parameters, sigma_2, N, regularization_terms='l1_starlet',
                    regularization_strength_scales=lambda_scales, regularization_strength_hf=lambda_hf,
                    regularization_strength_positivity=lambda_positivity, W=W,
                    regularize_full_psf=args.regularize_full_psf)

        optim = Optimizer(loss, parameters, method=method)
        best_fit, logL_best_fit, extra_fields, runtime = optim.minimize(
            max_iterations=n_iter, min_iterations=None,
            init_learning_rate=1e-2, schedule_learning_rate=True,
            restart_from_init=False, stop_at_loss_increase=False,
            progress_bar=True, return_param_history=True
        )

        loss_history = extra_fields['loss_history']
        param_history = extra_fields['param_history']
        kwargs_final = parameters.args2kwargs(best_fit)

        print()
        print("=== Results ===")
        print('Overall Reduced Chi2 : ', -2 * loss._log_likelihood_chi2(kwargs_final) / (image_size ** 2))
        print('Loss : ', loss.loss(best_fit))
        print('Log Likelihood : ', loss._log_likelihood(kwargs_final))
        print('Log Regul : ', loss._log_regul(kwargs_final))
        print('Log regul positivity :', loss._log_regul_positivity(kwargs_final))
        print()

    else :
        print('Moffat fit only, no background applied...')
        kwargs_final = kwargs_bestfit


    # Printing final results
    print('Final results :', kwargs_final)

    fig3 = ptf.single_PSF_plot(model, new_vignets_data, sigma_2, kwargs_final, n_psf=0, units='e-')
    fig4 = ptf.multiple_PSF_plot(model,new_vignets_data, sigma_2, kwargs_final, units='e-')
    if args.plot_interactive:
        plt.show()

    fig3.savefig(os.path.join(args.output_path, 'single_PSF_plot.png'))
    fig4.savefig(os.path.join(args.output_path, 'multit_PSF_plot.png'))

    if not args.no_bkg_fit:
        plt.figure(5)
        plt.plot(range(n_iter), loss_history)
        plt.xlabel('Steps')
        plt.ylabel('Loss')
        plt.savefig(os.path.join(args.output_path, 'Loss_history.png'))

    # Saving
    model.export(args.output_path, kwargs_final, new_vignets_data, sigma_2, format='fits')
    model.dump(os.path.join(args.output_path, 'model.pkl'), kwargs_final, norm)

if __name__ == '__main__':
    main(arguments)
