Installation
============


Requirements
------------

STARRED is developed using Python 3.8 and might be fine with older versions.

It requires ``astropy``, ``dill``, ``jax``, ``jaxlib``, ``matplotlib``, ``numpy``, ``scikit-image``, ``scipy``, ``optax``  and ``tqdm`` to work properly. 

Through Anaconda 
----------------

We provide an Anaconda environment that satisfies all the dependencies in ``starred-env.yml``.

::

    git clone https://gitlab.com/cosmograil/starred.git
    cd starred
    conda env create -f starred-env.yml
    conda activate starred-env
    pip install .

In case you have an NVIDIA GPU, this should automatically download the right version of JAX as well as cuDNN.
Next, you can run the tests to make sure your installation is working correctly.

::

    # While still in the starred directory:
    pytest . 

    
Manually handling the dependencies
----------------------------------

If you want to use an existing environment, just omit the Anaconda commands above:
::

    git clone https://gitlab.com/cosmograil/starred
    cd starred 
    pip install .


or if you need to install it for your user only:

::

	python setup.py install --user 
