starred\.deconvolution package
==============================

Submodules
----------

starred.deconvolution.deconvolution module
------------------------------------------

.. automodule:: starred.deconvolution.deconvolution
    :members:
    :undoc-members:
    :show-inheritance:

starred.deconvolution.loss module
---------------------------------

.. automodule:: starred.deconvolution.loss
    :members:
    :undoc-members:
    :show-inheritance:

starred.deconvolution.parameters module
---------------------------------------

.. automodule:: starred.deconvolution.parameters
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: starred.deconvolution
    :members:
    :undoc-members:
    :show-inheritance: