starred\.plots package
======================

Submodules
----------

starred.plots.plot_function module
----------------------------------

.. automodule:: starred.plots.plot_function
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: starred.plots
    :members:
    :undoc-members:
    :show-inheritance: