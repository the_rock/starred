starred\.utils package
======================

Submodules
----------

starred.utils.generic_utils module
----------------------------------

.. automodule:: starred.utils.generic_utils
    :members:
    :undoc-members:
    :show-inheritance:

starred.utils.inference_base module
-------------------------

.. automodule:: starred.utils.inference_base
    :members:
    :undoc-members:
    :show-inheritance:

starred.utils.jax_utils module
------------------------------

.. automodule:: starred.utils.jax_utils
    :members:
    :undoc-members:
    :show-inheritance:

starred.utils.noise_utils module
------------------------------

.. automodule:: starred.utils.noise_utils
    :members:
    :undoc-members:
    :show-inheritance:

starred.utils.optimization module
---------------------------------

.. automodule:: starred.utils.optimization
    :members:
    :undoc-members:
    :show-inheritance:

starred.utils.parameters module
-------------------------------

.. automodule:: starred.utils.parameters
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: starred.utils
    :members:
    :undoc-members:
    :show-inheritance: